var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var ClientSchema = new mongoose.Schema({
  client: {
    type: String,
    required: true,
    trim: true,
    unique: true,
  },
  page: {
    type: [],
    trim: true,
  },
  archive: {
    type: [],
    trim: true,
  },
  trade: {
    type: String,
    trim: true,
  },
  pageType: {
    type: [],
    trim: true,
  },
  vocLink: {
    type: String,
    trim: true,
  },
  vocPage: {
    type: String,
    trim: true,
  },
  webUrl: {
    type: String,
    trim: true,
  },
  webProvider: {
    type: String,
    trim: true,
  },
  creativeServices: {
    type: Boolean,
    default: false,
  },
  leadAttribution: {
    type: Boolean,
    default: false,
  },
  xchangeValue: {
    type: Boolean,
    default: false,
  },
  video: {
    type: Boolean,
    default: false,
  },
  chat: {
    type: Boolean,
    default: false,
  },
  chatType: {
    type: String,
    trim: true,
  },
  yext: {
    type: Boolean,
    default: false,
  },
  autoleadstar: {
    type: Boolean,
    default: false,
  },
  pulseType: {
    type: String,
    trim: true,
  },
  seo: {
    type: Boolean,
    default: false,
  }, 
  seoAnalyst: {
    type: String,
    trim: true,
  },
  socialElite: {
    type: Boolean,
    default: false,
  }, 
  classifieds: {
    type: Boolean,
    default: false,
  }, 
  sem: {
    type: Boolean,
    default: false,
  }, 
  reputation: {
    type: Boolean,
    default: false,
  }, 
  autoGroup: {
    type: String,
    trim: true,
  },
  analyticsID: {
    type: String,
    trim: true,
  },
  gtmID: {
    type: String,
    trim: true,
  }
});

var Pages = mongoose.model('Pages', ClientSchema);
module.exports = Pages;