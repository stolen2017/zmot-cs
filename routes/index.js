var express = require('express');
var router = express.Router();
var User = require('../model/user');
var Page = require('../model/pages');
var mid = require('../middleware');

// GET /profile
/*router.get('/profile', mid.reqLogin, function(req, res, next) {
  if (! req.session.userId ) {
    var err = new Error("You are not authorized to view this page.");
    err.status = 403;
    return next(err);
  }
  User.findById(req.session.userId)
      .exec(function (error, user) {
        if (error) {
          return next(error);
        } else {
          return res.render('profile', { title: 'Profile', name: user.name, favorite: user.favoriteBook });
        }
      });
});*/

//Get /logout
router.get('/logout', function(req, res, next){
	if(req.session){
		//delet session object
		req.session.destroy(function(err) {
			if(err){
				return next(err);
			} else {
				return res.redirect('/');
			}
		});
	}
});

//GET /login
router.get('/login', mid.loggedOut, function(req, res, next){
	return res.render('login', {title: 'Log In'});
});

//POST /login
router.post('/login', function(req, res, next){
	if(req.body.email && req.body.password){
		User.authenticate(req.body.email, req.body.password, function (error, user) {
			if (error || !user) {
				var err = new Error('Wrong email or password.');
				err.status = 401;
				return next(err);
			} else {
				req.session.userId = user._id;
				//return res.render('index', { title: 'Home' });
				return res.redirect('/');
			}
		});
	}else{
		var err = new Error('Email and password are required.');
		err.status = 401;
		return next(err);
	}
	//return res.send('Logged In!');
});

//GET /register
router.get('/register', mid.loggedOut, function(req, res, next) {
  //return res.send('Register today!');
  //return res.render('register', { title: 'Sign Up' });
  //return res.render('view-pages', { title: 'View Pages' });
});

//post /register
router.post('/register', function(req, res, next){
  //return res.send('User created!');
  if(req.body.email && 
		req.body.name &&
		req.body.favoriteBook &&
		req.body.password &&
		req.body.confirmPassword){
		  
			//confirm that user typed same password twice
		  if(req.body.password !== req.body.confirmPassword){
			  var err = new Error('Passwords do not match.');
			  err.status = 400;
			  return next(err);
	  	}
	  	
	  	//create object with form input
		  var userData = {
  			email: req.body.email,
  			name: req.body.name,
  			favoriteBook: req.body.favoriteBook,
  			password: req.body.password
		  };
		  
		  //use schema's 'create' method to insert document into mongo
		  User.create(userData, function(error, user){
  			if(error){
  				return next(error);
  			}else{
  				req.session.userId = user._id;
  				return res.render('index', { title: 'Home' });
  			}
		  });
		
	}else{
		var err = new Error('All fields required.');
		err.status = 400;
		return next(err);
			
	}
});

// GET /
router.get('/', function(req, res, next) {
  return res.render('index', { title: 'Home' });
});

// GET /view pages
router.get('/view-pages', function(req, res, next){
      var resultArray = [];
  	  
  	  Page.find({}, {client:true, page:true, archive:true}).sort({ client: 'ascending' }).cursor()
      //Page.find().cursor()
      	.on('data',  function(doc){
      		resultArray.push(doc)
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      	  return res.render('view-pages', {title: 'View Pages', dealers: resultArray});
      	})
	
});//end router.get view pages

// GET /view pages
router.get('/view-landing-pages-old', function(req, res, next){
      var resultArray = [];
  	  var options = {
    		"client": true,
    		"page": true,
    		"archive": true,
    		"sort": -1
			}
  	  Page.find({}, {client:true, page:true, archive:true}).sort({ client: 'ascending' }).cursor()
  	  //Page.find({}, options).cursor()
      //Page.find().cursor()
      	.on('data',  function(doc){
      	//.on('data').sort({client: -1}).toArray(function(doc){
      		resultArray.push(doc);
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      		//console.log(resultArray);
      	  return res.render('view-landing-pages-old', {title: 'View Landing Pages', dealers: resultArray});
      	})
});//end router.get view pages
router.get('/view-landing-pages', function(req, res, next){
      var resultArray = [];
  	  //Page.find({}, {client:true, page:true, archive:true, pageType:true}).cursor()
      //Page.find().cursor()
      Page.find({}, {client:true, page:true, archive:true, pageType:true}).sort({ client: 'ascending' }).cursor()
      	.on('data',  function(doc){
      		resultArray.push(doc);
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      	  return res.render('view-landing-pages', {title: 'View Landing Pages', dealers: resultArray});
      	})
});//end router.get view pages

// GET /view pages
router.get('/view-trade', function(req, res, next){
      var resultArray = [];
  	  
  	  Page.find({}, {client:true, trade:true}).sort({ client: 'ascending' }).cursor()    
      //Page.find().cursor()
      	.on('data',  function(doc){
      		resultArray.push(doc)
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      		//console.log(resultArray);
      	  return res.render('view-trade', {title: 'View Trade', dealers: resultArray});
      	})
	
});//end router.get view trade

// GET /view voc
router.get('/view-voc', function(req, res, next){
      var resultArray = [];
  	  
  	  Page.find({}, {client:true, vocLink:true, vocPage:true}).sort({ client: 'ascending' }).cursor()    
      //Page.find().cursor()
      	.on('data',  function(doc){
      		resultArray.push(doc)
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      	  //console.log(resultArray);
      	  return res.render('view-voc', {title: 'Voice of the Customer', dealers: resultArray});
      	})
	
});//end router.get view voc

// GET /add client
router.get('/add-client', function(req, res, next){
	if (! req.session.userId ) {
    var err = new Error("You are not authorized to view this page.");
    err.status = 403;
    return next(err);
  }else{
		return res.render('add-client', { title: 'Add Client' });
 }
	
});

//POST /add client
router.post('/add-client', function(req, res, next){
	//if(req.body.name && req.body.page && req.body.archive){
	if(req.body.name){
		//create object with form input
		  var clientData = {
  			client: req.body.name,
  			//page: req.body.page,
  			//archive: req.body.archive
		  };
		  
		  //console.log(clientData);
		  //use schema's 'create' method to insert document into mongo
		  Page.create(clientData, function(error, client){
  			if(error){
  				return next(error);
  			}else{
  				return res.render('index', { title: 'Home' });
  			}
		  });
		
	}else{
		
		var err = new Error('All fields required.');
		err.status = 400;
		return next(err);
	}
});

// GET /add page
router.get('/add-page', function(req, res, next){
	if (! req.session.userId ) {
    var err = new Error("You are not authorized to view this page.");
    err.status = 403;
    return next(err);
  }else{
  	
  	//var cursor = Page.find({client:true});
      var resultArray = [];
  	      
      Page.find({}, {client:true}).sort({ client: 'ascending' }).cursor()
      	.on('data',  function(doc){
      		resultArray.push(doc)
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      		//console.log(resultArray);
      	  return res.render('add-page', { title: 'Add Page', dealers: resultArray});
      	})
  	
		
  }
});//end router.get add-page

//POST /add page
router.post('/add-page', function(req, res, next){
	
	//if(req.body.name && req.body.page && req.body.archive && req.body.pageType){
	if(req.body.name && req.body.page && req.body.pageType){
		//create object with form input
		  var pageData = {
  			client: req.body.name,
  			page: req.body.page,
  			//archive: req.body.archive,
  			pageType: req.body.pageType
		  };
    	
    	var query = { client: pageData.client };
			//Page.findOneAndUpdate(query, {$addToSet: {page: pageData.page, archive: pageData.archive }},  function(error, client){
			Page.findOneAndUpdate(query, {$addToSet: {page: pageData.page }},  function(error, client){
			if(error){
  				return next(error);
  			}
			});
			Page.findOneAndUpdate(query, {$push: {pageType: pageData.pageType }},  function(error, client){
			if(error){
  				return next(error);
  			}else{
  				return res.render('index', { title: 'Home' });
  			}
			});
	}else{
		
		var err = new Error('All fields required.');
		err.status = 400;
		return next(err);
	}
});//end router.post

// GET /add trade
router.get('/add-trade', function(req, res, next){
  if (! req.session.userId ) {
    var err = new Error("You are not authorized to view this page.");
    err.status = 403;
    return next(err);
  }else{
  	
  	//var cursor = Page.find({client:true});
      var resultArray = [];
  	      
      Page.find({}, {client:true}).sort({ client: 'ascending' }).cursor()
      	.on('data',  function(doc){
      		resultArray.push(doc)
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      	  return res.render('add-trade', { title: 'Add Trade Tool Link', dealers: resultArray});
      	})
  	
		
  }
});//end router.get add-page

//POST /add trade
router.post('/add-trade', function(req, res, next){
	
	if(req.body.name && req.body.page){
		//create object with form input
		  var tradeData = {
  			client: req.body.name,
  			page: req.body.page,
		  };
    	
    	//console.log(tradeData);
    	var query = { client: tradeData.client };
			Page.findOneAndUpdate(query, {$set: { trade: tradeData.page }},  function(error, client){
				if(error){
  				return next(error);
  			}else{
  				return res.render('index', { title: 'Home' });
  			}
				
			});
		
	}else{
		
		var err = new Error('All fields required.');
		err.status = 400;
		return next(err);
	}
});//end router.post

// GET /add voc
router.get('/add-voc', function(req, res, next){
  if (! req.session.userId ) {
    var err = new Error("You are not authorized to view this page.");
    err.status = 403;
    return next(err);
  }else{
  	
  	//var cursor = Page.find({client:true});
      var resultArray = [];
  	      
      Page.find({}, {client:true}).sort({ client: 'ascending' }).cursor()
      	.on('data',  function(doc){
      		resultArray.push(doc)
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      	  return res.render('add-voc', { title: 'Voice of the Customer', dealers: resultArray});
      	})
  }
});//end router.get add-voc
//POST /add voc
router.post('/add-voc', function(req, res, next){
	
	if(req.body.name && req.body.vocLink){
		//create object with form input
		  var vocData = {
	  		client: req.body.name,
	  		vocLink: req.body.vocLink,
	  		vocPage: req.body.vocPage,
		  };
    	
    	var query = { client: vocData.client };
			Page.findOneAndUpdate(query, {$set: { vocLink: vocData.vocLink, vocPage: vocData.vocPage }},  function(error, client){
				if(error){
  					return next(error);
	  			}else{
	  				return res.render('index', { title: 'Home' });
	  			}
			});
		
	}else{
		var err = new Error('Dealer and VOC Link Required');
		err.status = 400;
		return next(err);
	}
});//end router.post

// GET /update-client
router.get('/update-services', function(req, res, next){
  if (! req.session.userId ) {
    var err = new Error("You are not authorized to view this page.");
    err.status = 403;
    return next(err);
  }else{
  	
  	//var cursor = Page.find({client:true});
      var resultArray = [];
  	      
      Page.find({}, {client:true}).sort({ client: 'ascending' }).cursor()
      	.on('data',  function(doc){
      		resultArray.push(doc)
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      	  return res.render('update-services', { title: 'Update Services', dealers: resultArray});
      	})
  	
		
  }
});//end router.get update-services
// GET /update-services
router.get('/update-info', function(req, res, next){
  if (! req.session.userId ) {
    var err = new Error("You are not authorized to view this page.");
    err.status = 403;
    return next(err);
  }else{
  	
  	//var cursor = Page.find({client:true});
      var resultArray = [];
  	      
      Page.find({}, {client:true}).sort({ client: 'ascending' }).cursor()
      	.on('data',  function(doc){
      		resultArray.push(doc)
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      	  return res.render('update-info', { title: 'Update Information', dealers: resultArray});
      	})
  	
		
  }
});//end router.get update-info

router.post('/update-services', function(req, res, next){
	
	if(req.body.name){
		//create object with form input
		  var clientData = {
	  		client: req.body.name,
	  		chat: req.body.chat,
	  		chatType: req.body.chatType,
	  		yext: req.body.yext,
	  		creative: req.body.creative,
	  		xchangeValue: req.body.xchangeValue,
	  		video: req.body.video,
	  		pulse: req.body.pulse,
	  		pulseType: req.body.pulseType,
	  		seo: req.body.seo,
	  		seoAnalyst: req.body.seoAnalyst,
	  		socialElite: req.body.socialElite,
	  		classifieds: req.body.classifieds,
	  		sem: req.body.sem,
	  		reputation: req.body.reputation,
		  };
		  
		  console.log(clientData);
		  
		  var query = { client: clientData.client };
		  
		  
		  if(clientData.chat){
		  	if(clientData.chatType){
		  		Page.findOneAndUpdate(query, {$set: { chat: clientData.chat, chatType: clientData.chatType }},  function(error, client){
					if(error){
	  					return next(error);
		  			}
				});
			} else {
				var err = new Error('If chat is marked chatType must also be selected');
				err.status = 400;
				return next(err);
			}
		  }
		  
		  if(clientData.yext){
		  	Page.findOneAndUpdate(query, {$set: { yext: clientData.yext }},  function(error, client){
				if(error){
  					return next(error);
	  			}
			});
		  }
		  
		  if(clientData.creative){
		  	Page.findOneAndUpdate(query, {$set: { creativeServices: clientData.creative }},  function(error, client){
				if(error){
  					return next(error);
	  			}
			});
		  }
		  
		  if(clientData.xchangeValue){
		  	Page.findOneAndUpdate(query, {$set: { xchangeValue: clientData.xchangeValue }},  function(error, client){
				if(error){
  					return next(error);
	  			}
			});
		  }
		  
		  if(clientData.video){
		  	Page.findOneAndUpdate(query, {$set: { video: clientData.video }},  function(error, client){
				if(error){
  					return next(error);
	  			}
			});
		  }
		  
		  if(clientData.pulse){
		  	if(clientData.pulseType){
		  		Page.findOneAndUpdate(query, {$set: { autoleadstar: clientData.pulse, pulseType: clientData.pulseType }},  function(error, client){
					if(error){
	  					return next(error);
		  			}
				});
			} else {
				var err = new Error('If pulse is marked pulseType must also be selected');
				err.status = 400;
				return next(err);
			}
		  }
		  
		  if(clientData.seo){
		  	if(clientData.seoAnalyst){
		  		Page.findOneAndUpdate(query, {$set: { seo: clientData.seo, seoAnalyst: clientData.seoAnalyst }},  function(error, client){
					if(error){
	  					return next(error);
		  			}
				});
			} else {
				var err = new Error('If seo is marked seo analyst must also be selected');
				err.status = 400;
				return next(err);
			}
		  }
		  
		  if(clientData.socialElite){
		  	Page.findOneAndUpdate(query, {$set: { socialElite: clientData.socialElite }},  function(error, client){
				if(error){
  					return next(error);
	  			}
			});
		  }
		  
		  if(clientData.classifieds){
		  	Page.findOneAndUpdate(query, {$set: { classifieds: clientData.classifieds }},  function(error, client){
				if(error){
  					return next(error);
	  			}
			});
		  }
		  
		  if(clientData.sem){
		  	Page.findOneAndUpdate(query, {$set: { sem: clientData.sem }},  function(error, client){
				if(error){
  					return next(error);
	  			}
			});
		  }
		  
		  if(clientData.reputation){
		  	Page.findOneAndUpdate(query, {$set: { reputation: clientData.reputation }},  function(error, client){
				if(error){
  					return next(error);
	  			}
			});
		  }
		  
		 return res.render('index', { title: 'Home' });
		 
	}else{
		var err = new Error('Dealer Required');
		err.status = 400;
		return next(err);
	}
});//end router.post

//POST /update-client
router.post('/update-info', function(req, res, next){
	
	if(req.body.name){
		//create object with form input
		  var clientData = {
	  		client: req.body.name,
	  		webUrl: req.body.webUrl,
	  		webProvider: req.body.webProvider,
	  		autoGroup: req.body.autoGroup,
	  		gaID: req.body.gaID,
	  		gtmID: req.body.gtmID,
		  };
		  
		  console.log(clientData);
		  
		  var query = { client: clientData.client };
		  
		  if(clientData.webUrl){
		  	if(clientData.webProvider){
		  		Page.findOneAndUpdate(query, {$set: { webUrl: clientData.webUrl, webProvider: clientData.webProvider }},  function(error, client){
					if(error){
	  					return next(error);
		  			}
				});
		  	} else {
				var err = new Error('Both WebUrl and WebProvider are co-dependent');
				err.status = 400;
				return next(err);
			}
		  }
		  
		  if(clientData.autoGroup){
		  	Page.findOneAndUpdate(query, {$set: { autoGroup: clientData.autoGroup }},  function(error, client){
				if(error){
  					return next(error);
	  			}
			});
		  }
		  
		  if(clientData.gaID){
		  	Page.findOneAndUpdate(query, {$set: { analyticsID: clientData.gaID }},  function(error, client){
				if(error){
  					return next(error);
	  			}
			});
		  }
		  
		  if(clientData.gtmID){
		  	Page.findOneAndUpdate(query, {$set: { gtmID: clientData.gtmID }},  function(error, client){
				if(error){
  					return next(error);
	  			}
			});
		  }
		  
		 return res.render('index', { title: 'Home' });
		 
	}else{
		var err = new Error('Dealer Required');
		err.status = 400;
		return next(err);
	}
});//end router.post

//GET view clients
router.get('/view-client', function(req, res, next){
		var resultArray = [];
	  	      
	      Page.find({}, {client:true}).sort({ client: 'ascending' }).cursor()
	      	.on('data',  function(doc){
	      		resultArray.push(doc)
	      	})
	      	.on('error', function(err){
	      		return next(err);
	      	})
	      	.on('end', function(){
	      	  return res.render('view-client', { title: 'View Client', dealers: resultArray});
	      	})
});//end router.get view client

router.get('/client-selected', function(req, res, next){
	if(req.query.name){
	      	
		var clientArray = [];
		
		Page.findOne({client: req.query.name}, {client:true, autoGroup:true, webUrl:true, webProvider:true, 
												creativeServices:true, leadAttribution:true,
												xchangeValue:true, video:true, chat:true, chatType:true, yext:true, autoleadstar:true, pulseType:true,
												seo:true, seoAnalyst:true, socialElite:true, classifieds: true, sem:true, reputation:true, 
												analyticsID:true, gtmID:true}).cursor()    
      	.on('data',  function(doc){
      		clientArray.push(doc)
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      	  //console.log(resultArray[0]['client']);
      	  //console.log(resultArray[0]['webProvider']);
      	  //console.log(resultArray[0]['leadAttribution']);
      	  //console.log(resultArray);
      	  //return res.render('view-client', {title: 'View Client', client: clientSelected, dealers: resultArray});
      	  //return res.render('/client-selected', { title: 'Client Selected' , client: clientSelected});
      	  //return res.redirect('/view-client', {client: clientSelected});
      	  //return res.render('/', {client: clientArray});
      	  //return res.render('index', { title: 'Home' });
      	  return res.render('client-selected', {title: 'Selected Client', clientSelected: clientArray});
      	})
		
	} else {
		var err = new Error('No query in url String');
		err.status = 400;
		return next(err);
	
	}
});//end router.get selected client

//GET view info
router.get('/view-info', function(req, res, next){
		var resultArray = [];
	  	      
	      Page.find({}, {client:true}).sort({ client: 'ascending' }).cursor()
	      	.on('data',  function(doc){
	      		resultArray.push(doc)
	      	})
	      	.on('error', function(err){
	      		return next(err);
	      	})
	      	.on('end', function(){
	      	  return res.render('view-info', { title: 'View Info', dealers: resultArray});
	      	})
});//end router.get view client

//GET view services
router.get('/view-services', function(req, res, next){
	var resultArray = [];
	  	      
      Page.find({}, {client:true}).sort({ client: 'ascending' }).cursor()
      	.on('data',  function(doc){
      		resultArray.push(doc)
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      		return res.render('view-services', { title: 'View Services', dealers: resultArray});
      	})
});//end router.get view client

router.get('/client-info-selected', function(req, res, next){
	if(req.query.name){
	      	
		var clientArray = [];
		
		Page.findOne({client: req.query.name}, {client:true, autoGroup:true, webUrl:true, webProvider:true, leadAttribution:true,
												chatType:true, yext:true, pulseType:true,
												seoAnalyst:true, analyticsID:true, gtmID:true}).cursor()    
      	.on('data',  function(doc){
      		clientArray.push(doc)
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      	  return res.render('client-info-selected', {title: 'Selected Client', clientSelected: clientArray});
      	})
		
	} else {
		var err = new Error('No query in url String');
		err.status = 400;
		return next(err);
	
	}
});//end router.get selected client

router.get('/client-services-selected', function(req, res, next){
	if(req.query.name){
	      	
		var clientArray = [];
		
		Page.findOne({client: req.query.name}, {client:true, creativeServices:true, xchangeValue:true, video:true, chat:true, autoleadstar:true,
												seo:true, socialElite:true, classifieds: true, sem:true, reputation:true}).cursor()    
      	.on('data',  function(doc){
      		clientArray.push(doc)
      	})
      	.on('error', function(err){
      		return next(err);
      	})
      	.on('end', function(){
      	  return res.render('client-services-selected', {title: 'Client Services Selected', clientSelected: clientArray});
      	})
		
	} else {
		var err = new Error('No query in url String');
		err.status = 400;
		return next(err);
	
	}
});//end router.get selected client services

module.exports = router;